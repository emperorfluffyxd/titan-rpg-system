import { titanRPG } from "./module/config.js";
import TitanItemSheet from "./module/sheets/TitanItemSheet.js";

Hooks.once("init", function() {
    console.log("TitanRPG | Initializing Titam RPG System");

    CONFIG.titanRPG = titanRPG;

    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("titan-rpg-system", TitanItemSheet);
});