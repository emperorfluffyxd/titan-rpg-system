export const titanRPG = {
    attributeDesc: "titanRPG.attributeDesc",
    attributesDesc: "titanRPG.attributesDesc",
    attributes: {
        body: "titanRPG.attributes.body",
        mind: "titanRPG.attributes.mind",
        soul: "titanRPG.attributes.soul"
    },
    resistanceDesc: "titanRPG.resistanceDesc",
    resistancesDesc: "titanRPG.resistancesDesc",
    resistances: {
        fortitude: "titanRPG.resistances.fortitude",
        reflexes: "titanRPG.resistances.reflexes",
        willpower: "titanRPG.resistances.willpower"
    },
    skillDesc: "titanRPG.skillDesc",
    skillsDesc: "titanRPG.skillsDesc",
    skills: {
        arcana: "titanRPG.skills.arcana",
        athletics: "titanRPG.skills.athletics",
        acrobatics: "titanRPG.skills.acrobatics",
        ballistics: "titanRPG.skills.ballistics",
        beast: "titanRPG.skills.beastHandling",
        crafting: "titanRPG.skills.crafting",
        deception: "titanRPG.skills.deception",
        devotion: "titanRPG.skills.devotion",
        diplomacy: "titanRPG.skills.diplomacy",
        martial: "titanRPG.skills.martial",
        medicine: "titanRPG.skills.medicine",
        perception: "titanRPG.skills.perception",
        survival: "titanRPG.skills.survival",
        thievery: "titanRPG.skills.thievery",
        theology: "titanRPG.skills.theology"
    },
    attackTypeDesc: "titanRPG.attackTypeDesc",
    attackTypes: {
        melee: "titanRPG.attackTypes.melee",
        ranged: "titanRPG.attackTypes.ranged"
    },
    rangeDesc: "titanRPG.rangeDesc",
    ranges: {
        close: "titanRPG.ranges.close",
        short: "titanRPG.ranges.short",
        medium: "titanRPG.ranges.medium",
        long: "titanRPG.ranges.long",
        extreme: "titanRPG.ranges.extreme"
    },
    damageDesc: "titanRPG.damageDesc",
    damageTypeDesc: "titanRPG.damageTypeDesc",
    damageTypes: {
        slashing: "titanRPG.damageTypes.slashing",
        piercing: "titanRPG.damageTypes.piercing",
        bludgeoning: "titanRPG.damageTypes.bludgeoning",
        force: "titanRPG.damageTypes.force",
        fire: "titanRPG.damageTypes.fire",
        cold: "titanRPG.damageTypes.cold",
        lightning: "titanRPG.damageTypes.lightning",
        necrotic: "titanRPG.damageTypes.necrotic",
        radiant: "titanRPG.damageTypes.radiant"
    },
    valueDesc: "titanRPG.value"
};