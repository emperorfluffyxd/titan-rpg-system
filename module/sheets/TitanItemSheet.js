export default class TitanItemSheet extends ItemSheet {
    get template() {
        return `systems/titan-rpg-system/templates/sheets/${this.item.data.type}-sheet.html`;
    }

    getData() {
        const baseData = super.getData();
        let sheetData = {
            owner: this.item.isOwner,
            editable: this.isEditable,
            item: baseData.item,
            data: baseData.item.data.data,
            config: CONFIG.titanRPG
        };

        return sheetData;
    }
}